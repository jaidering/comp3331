import java.io.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.*;

public class mtp_receiver {
	static int isn;
	public static void main(String args[]) throws Exception {
		int myPort = Integer.parseInt(args[0]);
		String fileName = args[1];
		DatagramSocket socket = new DatagramSocket(myPort);
		Random rand = new Random();
		isn = rand.nextInt();
		int sendport = 6001;
		long isn = 9000;
		long MWS = 0;
		long MSS = 0;
		long sendbase = 0;

		File file = new File(fileName);

		if (!file.exists())
			file.createNewFile();
		while (true) {
			DatagramPacket dPack = new DatagramPacket(new byte[68], 68);	
			socket.receive(dPack);
			System.out.println("---received---");
			showPacket(dPack);
			System.out.println("-------------");
			InetAddress clientHost = dPack.getAddress();
			int clientPort = dPack.getPort();
			String packetType = getPacketType(dPack);
			if(packetType.equals("syn")){
				MWS = getMWS(dPack);
				MSS = getMSS(dPack);
				System.out.println("MWS:" + MWS + "MSS" + MSS);
				byte[] synackpack = handshakePacket(sendport, dPack.getPort(), isn, getSeq(dPack) + 1, "synack",MWS,MSS);
				DatagramPacket synack = new DatagramPacket(synackpack, synackpack.length,clientHost, clientPort);
				writeLog("receive SYN packet",dPack);
				socket.send(synack);
				writeLog("send SYN ACK", synack);
			}else if (packetType.equals("ackpsh")){
				long seq = getSeq(dPack); 
				byte[] ackcontent;

				if(seq != sendbase){
					ackcontent = ackPacket(sendport, dPack.getPort(), isn+1, sendbase);
					DatagramPacket pk = new DatagramPacket(ackcontent, ackcontent.length,clientHost, clientPort);			
					writeLog("receive ACKPSH packet",dPack);
					socket.send(pk);				
					System.out.println("1send ACK back, ack: " + sendbase);
					writeLog("response ACK back",pk);
				}
			}else if(packetType.equals("fin")){
				writeLog("receive FIN packet",dPack);
				socket.close();
				System.exit(0);
			}
		}
	}

	public static void showbyte(byte[] filepart){
		int c = 0;
		for (byte b : filepart) {
			int ival = (b & 0xff);
			String t = Integer.toBinaryString(ival);
			int len = t.length();
			for (int i = 0; i < 8 - len; i++) {
				t = "0" + t;
			}
			if(c % 4 == 0 && c!= 0)
				System.out.print("\n");
			c++;
			System.out.print(t + "--");
		}
		System.out.println();
	}

	public static byte[] handshakePacket(int srcPort, int destPort, long seq, long ack, String flag, long MWS, long MSS){
		byte[] p = new byte[18];
		p[0] = (byte) (srcPort >>> 8); 
		p[1] = (byte) (srcPort >>> 0);
		p[2] = (byte) (destPort >>> 8);
		p[3] = (byte) (destPort >>> 0);
		p[4] = (byte)(seq >>> 32);
		p[5] = (byte)(seq >>> 16);
		p[6] = (byte)(seq >>> 8);
		p[7] = (byte)(seq >>> 0);
		p[8] = (byte)(ack >>> 32);
		p[9] = (byte)(ack >>> 16);
		p[10] = (byte)(ack >>> 8);
		p[11] = (byte)(ack >>> 0);
		p[12] = (byte)(0 >>> 0);
		if(flag.equals("syn")){
			p[13] = (byte)(2 >>> 0); 
		}else if(flag.equals("ack")){
			p[13] = (byte)(64 >>> 0); 
		}else if(flag.equals("fin")){
			p[13] = (byte)(1 >>> 0); 
		}else if(flag.equals("synack")){
			p[13] = (byte) (66 >>> 0);
		}
		p[14] = (byte)(MWS >>> 8);
		p[15] = (byte)(MWS >>> 0);
		p[16] = (byte)(MSS >>> 8);
		p[17] = (byte)(MSS >>> 0);
		return p;
	}

	private static void writeLog(String event, DatagramPacket pack) throws IOException{
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS");
		String tstr = formatter.format(new Date(System.currentTimeMillis()));
		String packtype = getPacketType(pack);
		String data = null;

		byte[] allcontent = pack.getData();
		byte[] dataContent = new byte[allcontent.length - 18];
		FileWriter fstream = new FileWriter("mtp_receiver_log",true);
		BufferedWriter out = new BufferedWriter(fstream);
		long seq = getSeq(pack);
		long ack = getAck(pack);
		if(packtype.equals("syn")){
			data = null;
			out.write(tstr + " " + event + "\n");
		}else if(packtype.equals("ack")){
			out.write(tstr + " " + event + ": " + seq + " " + ack + " " + " =====> " + data + "\n");
		}else if(packtype.equals("fin")){
			out.write(tstr + " " + event + "\n");
		}else if(packtype.equals("synack")){
			out.write(tstr + " " + event + "\n");
		}else if(packtype.equals("ackpsh")){

			dataContent = new byte[allcontent.length - 18];
			for(int j = 18; j < allcontent.length; j++){
				dataContent[j-18] = allcontent[j];
			}
			ByteArrayInputStream bais = new ByteArrayInputStream(dataContent);
			InputStreamReader isr = new InputStreamReader(bais);
			BufferedReader br = new BufferedReader(isr);
			String s;
			StringBuilder sb = new StringBuilder();
			while((s = br.readLine()) != null){
				sb.append(s);
			}
			data = sb.toString();
			out.write(tstr + " " + event + ": " + seq + " " + ack + " " + " =====> " + data + "\n");
			bais.close();
			isr.close();
		}
		out.close();
	}

	private static byte[] ackPacket(int srcPort, int destPort, long seq, long ack) {
		byte[] p = new byte[18]; 
		p[0] = (byte) (srcPort >>> 8); 
		p[1] = (byte) (srcPort >>> 0);
		p[2] = (byte) (destPort >>> 8);
		p[3] = (byte) (destPort >>> 0);
		p[4] = (byte)(seq >>> 32);
		p[5] = (byte)(seq >>> 16);
		p[6] = (byte)(seq >>> 8);
		p[7] = (byte)(seq >>> 0);
		p[8] = (byte)(ack >>> 32);
		p[9] = (byte)(ack >>> 16);
		p[10] = (byte)(ack >>> 8);
		p[11] = (byte)(ack >>> 0);
		p[12] = (byte)(0 >>> 0);
		p[13] = (byte)(64 >>> 0); 
		p[14] = (byte)(0 >>> 8);
		p[15] = (byte)(0 >>> 0);
		p[16] = (byte)(0 >>> 8);
		p[17] = (byte)(0 >>> 0);

		return p;
	}

	private static String getPacketType(DatagramPacket dPack) {
		String r = new String();
		byte[] buffer = dPack.getData();

		byte b13 = buffer[13];
		if (   ((b13 & (1 << 6)) != 0) 
				&& ((b13 & (1 << 1)) != 0) ) {
			r = "synack";
		} if ((b13 & (1 << 6)) != 0) {
			r = "ack";				
		}else if ((b13 & (1 << 1)) != 0) {
			r = "syn";
		}
		return r;
	}

	public static void showPacket(DatagramPacket dPack){
		String packetType = getPacketType(dPack);
		System.out.println("Type: " + packetType);
		long seq = getSeq(dPack);
		long ack = getAck(dPack);
		System.out.println("seq#: " + seq);
		System.out.println("ack#: " + ack);
		showbyte(dPack.getData());
	}

	private static long getMWS(DatagramPacket dPack) {
		long MSS = 0L;
		byte[] buffer = dPack.getData();
		MSS <<= 8;
		MSS |= (long)buffer[14] & 0xFF;
		MSS <<= 8;
		MSS |= (long)buffer[15] & 0xFF;
		return MSS;
	}

	private static long getMSS(DatagramPacket dPack) {
		long MSS = 0L;
		byte[] buffer = dPack.getData();
		MSS <<= 8;
		MSS |= (long)buffer[16] & 0xFF;
		MSS <<= 8;
		MSS |= (long)buffer[17] & 0xFF;
		return MSS;
	}

	private static long getAck(DatagramPacket dPack) {
		long sq = 0L;
		byte[] b = dPack.getData();
		byte[] buffer = new byte[]{ b[8], b[9], b[10], b[11] };
		for (int i = 0; i < 4; i++) {
			sq <<= 8;
			sq |= (long)buffer[i] & 0xFF;
		}
		return sq;
	}

	private static long getSeq(DatagramPacket dPack) {
		long sq = 0;
		byte[] b = dPack.getData();
		byte[] buffer = new byte[]{ b[4], b[5], b[6], b[7] };
		for (int i = 0; i < 4; i++) {
			sq <<= 8;
			sq |= (long)buffer[i] & 0xFF;
		}
		return sq;
	}
}
