import java.io.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.*;

public class mtp_sender {
	String hostIp;
	InetAddress recPort;
	static String fileName;
	int MWS;
	int MSS;
	int timeout;
	float pdrop;
	int seed;
	static BufferedWriter bw;
	static final int FILE_SIZE = 1024 * 5;
	static final int SOCKET_TIMEOUT = 150000;	
	static Calendar startTime = Calendar.getInstance();
	static Calendar currentTime = Calendar.getInstance();
	static boolean connectionEstablished = false;
	static DatagramSocket socket;
	Thread timerthread = null;
	public mtp_sender(String hostIp, InetAddress recIp, String fileName,
			int MWS, int MSS, int timeout, float pdrop, int seed) {
		try{
			socket = new DatagramSocket(6000);
		}catch (SocketException e)
		{
			e.printStackTrace();
		}

		this.hostIp = hostIp;
		this.recPort = recIp;
		mtp_sender.fileName = fileName;
		this.MWS = MWS;
		this.MSS = MSS;
		this.timeout = timeout;
		this.pdrop = pdrop;
		this.seed = seed;
	}

	public Thread getTimerthread() {
		return timerthread;
	}

	public void setTimerthread(Thread timerthread) {
		this.timerthread = timerthread;
	}

	public static void main(String[] args) throws Exception {
		InetAddress recIp = InetAddress.getByName(args[0]);		
		int recPort = Integer.parseInt(args[1]);
		String filename = args[2];
		int MWS = Integer.parseInt(args[3]);
		int MSS = Integer.parseInt(args[4]);
		int timeout = Integer.parseInt(args[5]);
		float pdrop = Float.parseFloat(args[6]);
		int seed = Integer.parseInt(args[7]);
		Random rand = new Random(seed);
		new mtp_sender("localhost", recIp, filename, MWS, MSS, timeout, pdrop, seed);
		int sendport = 6000;
		long client_isn = 8000;
		long server_isn = 0;
		client_isn = Math.abs(rand.nextInt());
		socket = new DatagramSocket();
		socket.setSoTimeout(timeout);
		File file = new File(fileName);
		new FileInputStream(file);
		bw = new BufferedWriter(new FileWriter("mtp_sender_log.txt"));
		bw.write(" ");

		byte[] synBuff= handshakePacket(sendport, 6001, client_isn, 0, "syn", MWS,MSS);
		DatagramPacket sendPacket = new DatagramPacket(synBuff, synBuff.length, recIp, recPort);
		mtp_sender.socket.send(sendPacket);
		writeLog("send SYN",sendPacket);
		while(true){
			DatagramPacket recvpacket = new DatagramPacket(new byte[18], 18);
			mtp_sender.socket.receive(recvpacket);
			writeLog("receive SYN ACK packet",recvpacket);
			server_isn = getseq(recvpacket);
			String pt = getPacketType(recvpacket);		
			if(pt.equals("synack")){				
				byte[] syn0 = handshakePacket(sendport, recvpacket.getPort(), client_isn + 1, server_isn + 1, "ack", MWS,MSS);
				DatagramPacket syn0Packet = new DatagramPacket(syn0, syn0.length, recIp, recPort);
				mtp_sender.socket.send(syn0Packet);
				writeLog("send last SYN packet",syn0Packet);
				break;
			}			
		}
	}

	public static void writeLog(String event, DatagramPacket pack) throws IOException{
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS");
		long timeInMillis = System.currentTimeMillis();
		String tstr = formatter.format(new Date(timeInMillis));
		String packtype = getPacketType(pack);
		String data = null;
		byte[] allcontent = pack.getData();
		byte[] datacontent = new byte[allcontent.length - 18];
		FileWriter fstream = new FileWriter("mtp_sender.log",true);
		BufferedWriter out = new BufferedWriter(fstream);

		if(packtype.equals("syn")){

			datacontent = null;
			out.write(tstr + " " + event + "\n");
		}else if(packtype.equals("ack")){
			out.write(tstr + " " + event + "\n");
		}else if(packtype.equals("fin")){
			out.write(tstr + " " + event + "\n");
		}else if(packtype.equals("synack")){
			out.write(tstr + " " + event + "\n");
		}else if(packtype.equals("ackpsh")){
			datacontent = new byte[allcontent.length - 18];
			for(int j = 18; j < allcontent.length; j++){
				datacontent[j-18] = allcontent[j];
			}
			long seq = getseq(pack);
			long ack = getack(pack);
			ByteArrayInputStream bais = new ByteArrayInputStream(datacontent);
			InputStreamReader isr = new InputStreamReader(bais);
			BufferedReader br = new BufferedReader(isr);
			String s;
			StringBuilder sb = new StringBuilder();
			while((s = br.readLine()) != null){
				sb.append(s);
			}
			data = sb.toString();
			out.write(tstr + " " + event + ": " + seq + " " + ack + " " + " =====> " + data + "\n");
			bais.close();
			isr.close();
		}
		out.close();
	}

	public static long getack(DatagramPacket request) {
		long sn = 0L;
		byte[] raw = request.getData();
		byte[] buf = new byte[]{ raw[8], raw[9], raw[10], raw[11] };
		for (int i = 0; i < 4; i++) {
			sn <<= 8;
			sn |= (long)buf[i] & 0xFF;
		}
		return sn;
	}

	public static long getseq(DatagramPacket request) {
		long sn = 0L;
		byte[] raw = request.getData();
		byte[] buf = new byte[]{ raw[4], raw[5], raw[6], raw[7] };
		for (int i = 0; i < 4; i++) {
			sn <<= 8;
			sn |= (long)buf[i] & 0xFF;
		}
		return sn;
	}

	public static byte[] handshakePacket(int srcPort, int destPort, long seq, long ack, String flag, int mws, int mss){
		byte[] p = new byte[18];
		p[0] = (byte) (srcPort >>> 8); 
		p[1] = (byte) (srcPort >>> 0);
		p[2] = (byte) (destPort >>> 8);
		p[3] = (byte) (destPort >>> 0);
		p[4] = (byte)(seq >>> 32);
		p[5] = (byte)(seq >>> 16);
		p[6] = (byte)(seq >>> 8);
		p[7] = (byte)(seq >>> 0);
		p[8] = (byte)(ack >>> 32);
		p[9] = (byte)(ack >>> 16);
		p[10] = (byte)(ack >>> 8);
		p[11] = (byte)(ack >>> 0);
		p[12] = (byte)(0 >>> 0);
		if(flag.equals("syn")){
			p[13] = (byte)(2 >>> 0); 
		}else if(flag.equals("ack")){
			p[13] = (byte)(64 >>> 0);
		}else if(flag.equals("fin")){
			p[13] = (byte)(1 >>> 0); 
		}else if(flag.equals("synack")){
			p[13] = (byte) (66 >>> 0);
		}
		p[14] = (byte)(mws >>> 8);
		p[15] = (byte)(mws >>> 0);
		p[16] = (byte)(mss >>> 8);
		p[17] = (byte)(mss >>> 0);
		return p;
	}

	public static String getPacketType(DatagramPacket request) {
		String r = new String();
		byte[] buf = request.getData();
		byte b13 = buf[13];
		if(((b13 & (1 << 6)) != 0) && ((b13 & (1 << 5)) != 0) ){
			r = "ackpsh";
		}else if (((b13 & (1 << 6)) != 0) && ((b13 & (1 << 1)) != 0) ) {			
			r = "synack";
		}else if ((b13 & (1 << 6)) != 0) {			
			r = "ack";
		}else if ((b13 & (1 << 0)) != 0) {
			r = "fin";		
		}else if ((b13 & (1 << 1)) != 0) {
			r = "syn";
		}
		return r;
	}
}